package com.volokh.list_visibility_utils.items;

import com.volokh.list_visibility_utils.utils.Config;
import com.volokh.list_visibility_utils.utils.LogUtils;
import com.volokh.list_visibility_utils.utils.Logger;
import ohos.agp.components.Component;

import java.util.List;

public class ListItemData {

    private static final boolean SHOW_LOGS = Config.SHOW_LOGS;
    private static final String TAG = ListItemData.class.getSimpleName();

    private  Integer mIndexInAdapter;
    private Component mView;

    private boolean mIsMostVisibleItemChanged;

    public int getIndex() {
        return mIndexInAdapter;
    }

    public Component getView() {
        return mView;
    }

    public ListItemData fillWithData(int indexInAdapter, Component view) {
        LogUtils.error("ListItemData", "indexInAdapter = " + indexInAdapter + " view = " + view);
        mIndexInAdapter = indexInAdapter;
        mView = view;
        return this;
    }

    public boolean isAvailable() {
        boolean isAvailable = mIndexInAdapter != null && mView != null;
        if (SHOW_LOGS) Logger.v(TAG, "isAvailable " + isAvailable);
        return isAvailable;
    }

    public int getVisibilityPercents(List<? extends ListItem> listItems) {
        LogUtils.info(TAG, "getIndex = " + getIndex());
        LogUtils.info(TAG, " getView = " + getView());
        LogUtils.info(TAG, " getView = " + listItems.get(getIndex()).getVisibilityPercents(getView()));
        int visibilityPercents = listItems.get(getIndex()).getVisibilityPercents(getView());
        if (SHOW_LOGS) Logger.v(TAG, "getVisibilityPercents, visibilityPercents " + visibilityPercents);
        return visibilityPercents;
    }

    public void setMostVisibleItemChanged(boolean isDataChanged) {
        mIsMostVisibleItemChanged = isDataChanged;
    }

    public boolean isMostVisibleItemChanged() {
        return mIsMostVisibleItemChanged;
    }

    @Override
    public String toString() {
        return "ListItemData{"
                + "mIndexInAdapter="
                + mIndexInAdapter
                + ", mView=" + mView
                + ", mIsMostVisibleItemChanged="
                + mIsMostVisibleItemChanged
                + '}';
    }
}
