package com.volokh.list_visibility_utils.utils;


import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class Logger {

    private static HiLogLabel hiLogLabel;

    public static int error(String TAG, final String message) {
        hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x00201, TAG);
        return HiLog.error(hiLogLabel, message);
    }

    public static int warn(String TAG, final String message) {
        hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x00201, TAG);
        return HiLog.warn(hiLogLabel, message);
    }

    public static int info(String TAG, final String message) {
        hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x00201, TAG);
        return HiLog.warn(hiLogLabel, message);
    }

    public static int debug(String TAG, final String message) {
        hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x00201, TAG);
        return HiLog.debug(hiLogLabel, message);
    }

    public static int v(String TAG, final String message) {
        hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x00201, TAG);
        return HiLog.info(hiLogLabel, message);
    }
}
