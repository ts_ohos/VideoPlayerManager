package com.volokh.list_visibility_utils.view;

import com.volokh.list_visibility_utils.calculator.AbsListView;
import com.volokh.list_visibility_utils.utils.LogUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.multimodalinput.event.TouchEvent;

/**
 * 自定义ListContainer 实现自定义的接口实现.
 * 2021/3/15
 */
public class ListView extends ListContainer implements Component.TouchEventListener, Component.ScrolledListener {
    public static final int Scroll_END = 1;

    private long time;

    private MyEventHandler myEventHandler;

    public ListView(Context context) {
        super(context);
        setScrolledListener(this);
        setTouchEventListener(this);
        myEventHandler = new MyEventHandler(EventRunner.create());
    }

    public ListView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        setScrolledListener(this);
        setTouchEventListener(this);
        myEventHandler = new MyEventHandler(EventRunner.create());
    }

    public ListView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    private boolean isTouchOver = false;

    private AbsListView.OnScrollListener onScrollListener = null;

    private class MyEventHandler extends EventHandler {
        private MyEventHandler(EventRunner runner) {
            super(runner);
        }

        // 重写实现processEvent方法
        @Override
        public void processEvent(InnerEvent event) {
            super.processEvent(event);
            if (event == null) {
                return;
            }
            int eventId = event.eventId;
            long param = event.param;
            switch (eventId) {
                case Scroll_END:
                    if (event.object instanceof AbsListView.OnScrollListener) {
                        LogUtils.info("MyeventHandler", "AbsListView.OnScrollListener.SCROLL_STATE_IDLE");
                        onScrollListener.onScrollStateChanged(ListView.this, AbsListView.OnScrollListener.SCROLL_STATE_IDLE);
                    }
                    // 待执行的操作，由开发者定义
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onContentScrolled(Component component, int newX, int newY, int oldX, int oldY) {
        String TAG = "onContentScrolled";
        LogUtils.info(TAG, "newX = " + newX + " newY = " + newY +
                "oldX = " + oldX + " oldY = " + oldY + "isTouchOver = " + isTouchOver);

        myEventHandler.removeEvent(1);
        InnerEvent event = InnerEvent.get(1, 0, onScrollListener);
        myEventHandler.sendEvent(event, 500);
        synchronized (this) {
            if (!isTouchOver)
                onScrollListener.onScrollStateChanged(this, AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL);
            else {
                onScrollListener.onScrollStateChanged(this, AbsListView.OnScrollListener.SCROLL_STATE_FLING);
            }
            onScrollListener.onScroll(this, getFirstVisibleItemPosition(), getLastVisibleItemPosition() - getFirstVisibleItemPosition() + 1,
                    getItemProvider().getCount());
        }

    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {

        /*        int oldY = 0, newY = 0;*/
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                /*         oldY = (int) getPivotY();
                 */
                isTouchOver = false;
                LogUtils.info("onTouchEvent", "down");
                break;
            case TouchEvent.POINT_MOVE:
           /*     oldY = newY;
                newY =(int) getPivotY();*/
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                /*  newY = (int) getPivotY();*/
                isTouchOver = true;
                LogUtils.info("onTouchEvent", "up");
                break;
            default:
                break;
        }


        return true;
    }

    public void setScrolledListener(AbsListView.OnScrollListener onScrollListener) {
        this.onScrollListener = onScrollListener;
    }

    @Override
    public int getChildCount() {
        return getLastVisibleItemPosition() - getFirstVisibleItemPosition() + 1;
    }

  /*  @Override
    public Component getComponentAt(int index) {
        Object allList = getItemProvider().getItem(0);
        if (allList instanceof List) {
            LogUtils.info("ListView", "List size = " + ((List) allList).size());
            LogUtils.info("ListView", "getFirstVisibleItemPosition() + index" + (getFirstVisibleItemPosition() + index));
            Object item =  ((List) allList).get(getFirstVisibleItemPosition() + index);
            if (item instanceof Component) {
                return ((Component) item);
            }
         }
        return null;
    }*/


}
