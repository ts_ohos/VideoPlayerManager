package com.volokh.list_visibility_utils.scroll_utils;


import com.volokh.list_visibility_utils.utils.Config;
import com.volokh.list_visibility_utils.utils.Logger;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.ScrollView;

/**
 * Using this class is can access all the data from RecyclerComponent
 * Created by danylo.ohos on 06.01.2016.
 */
public class RecyclerViewItemPositionGetter implements ItemsPositionGetter {

    private static final boolean SHOW_LOGS = Config.SHOW_LOGS;
    private static final String TAG = RecyclerViewItemPositionGetter.class.getSimpleName();

    private DirectionalLayoutManager mLayoutManager;
    private ScrollView mRecyclerComponent;

    public RecyclerViewItemPositionGetter(DirectionalLayoutManager layoutManager, ScrollView recyclerComponent) {
        mLayoutManager = layoutManager;
        mRecyclerComponent = recyclerComponent;
    }

    @Override
    public Component getChildAt(int position) {
        if (SHOW_LOGS) {
            Logger.v(TAG, "getChildAt, mRecyclerComponent.getChildCount " + mRecyclerComponent.getChildCount());
            //Logger.v(TAG, "getChildAt, mLayoutManager.getChildCount " + mLayoutManager.getChildCount());
        }

        //   Component Component = mLayoutManager.getChildAt(position);

        if (SHOW_LOGS) {
            //   Logger.v(TAG, "mRecyclerComponent getChildAt, position " + position + ", Component " + Component);
            //   Logger.v(TAG, "mLayoutManager getChildAt, position " + position + ", Component " + mLayoutManager.getChildAt(position));
        }

        return null;
    }

    @Override
    public int indexOfChild(Component Component) {
        //  int indexOfChild = mRecyclerComponent.indexOfChild(Component);
        //   if(SHOW_LOGS) Logger.v(TAG, "indexOfChild, " + indexOfChild);
        //      return indexOfChild;
        return 0;
    }

    @Override
    public int getChildCount() {
        int childCount = mRecyclerComponent.getChildCount();
        if (SHOW_LOGS) {
            Logger.v(TAG, "getChildCount, mRecyclerComponent " + childCount);
            //   Logger.v(TAG, "getChildCount, mLayoutManager " + mLayoutManager.getChildCount());
        }
        return childCount;
    }

    @Override
    public int getLastVisiblePosition() {
        //return mLayoutManager.findLastVisibleItemPosition();
        return 0;
    }

    @Override
    public int getFirstVisiblePosition() {
        //  if(SHOW_LOGS) Logger.v(TAG, "getFirstVisiblePosition, findFirstVisibleItemPosition " + mLayoutManager.findFirstVisibleItemPosition());
        // return mLayoutManager.findFirstVisibleItemPosition();
        return 0;
    }
}
