package com.volokh.list_visibility_utils.scroll_utils;


import ohos.agp.components.Component;

/**
 * Using this class is can access all the data from RecyclerView / ListView
 *
 * There is two different implementations for ListView and for RecyclerView.
 * RecyclerView introduced LayoutManager that's why some of data moved there
 *
 * Created by danylo.ohos on 9/20/2015.
 */
public interface ItemsPositionGetter {
    Component getChildAt(int position);

    int indexOfChild(Component view);

    int getChildCount();

    int getLastVisiblePosition();

    int getFirstVisiblePosition();
}
