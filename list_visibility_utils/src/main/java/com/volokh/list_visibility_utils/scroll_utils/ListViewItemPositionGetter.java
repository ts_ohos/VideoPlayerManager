package com.volokh.list_visibility_utils.scroll_utils;


import com.volokh.list_visibility_utils.utils.LogUtils;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;

/**
 * Using this class is can access all the data from ListView
 * <p>
 * Created by danylo.ohos on 1/17/2016.
 */
public class ListViewItemPositionGetter implements ItemsPositionGetter {

    private final static String TAG = "ListViewItemPositionGetter";

    private final ListContainer mListView;


    public ListViewItemPositionGetter(ListContainer listView) {
        mListView = listView;
    }


    @Override
    public Component getChildAt(int position) {
        LogUtils.error("ListViewItemPositionGetter", "position = " + (position));
        position = position <= mListView.getFirstVisibleItemPosition() ? mListView.getFirstVisibleItemPosition() : position;
        position = position > mListView.getLastVisibleItemPosition() ? mListView.getLastVisibleItemPosition() : position;
        LogUtils.error("ListViewItemPositionGetter", "view = " + mListView.getComponentAt(position));
        return mListView.getComponentAt(position);
    }

    @Override
    public int indexOfChild(Component view) {

        LogUtils.error(TAG, "first = " + mListView.getFirstVisibleItemPosition() + " last = " + mListView.getLastVisibleItemPosition());
        for (int i = mListView.getFirstVisibleItemPosition(); i <= mListView.getLastVisibleItemPosition(); i++) {
            Component view1 = mListView.getComponentAt(i);
            LogUtils.error(TAG, "i = " + i);
            if (view1 == view) return i;
        }
        return mListView.getFirstVisibleItemPosition();

    }

    @Override
    public int getChildCount() {
        LogUtils.error(TAG, "getChildCount" + mListView.getChildCount() + "");
        return mListView.getChildCount();
    }

    @Override
    public int getLastVisiblePosition() {
        LogUtils.error(TAG, "getLastVisiblePosition" + mListView.getLastVisibleItemPosition());
        return mListView.getLastVisibleItemPosition();

    }

    @Override
    public int getFirstVisiblePosition() {
        LogUtils.error(TAG, "get" + mListView.getFirstVisibleItemPosition());
        return mListView.getFirstVisibleItemPosition();
    }
}
