package com.ohos.vpmanager.visibility_demo.fraction;

import com.ohos.vpmanager.ResourceTable;
import com.ohos.vpmanager.visibility_demo.adapter.VisibilityUtilsAdapter;
import com.ohos.vpmanager.visibility_demo.adapter.items.VisibilityItem;
import com.volokh.list_visibility_utils.calculator.AbsListView;
import com.volokh.list_visibility_utils.calculator.DefaultSingleItemCalculatorCallback;
import com.volokh.list_visibility_utils.calculator.ListItemsVisibilityCalculator;
import com.volokh.list_visibility_utils.calculator.SingleListViewItemActiveCalculator;
import com.volokh.list_visibility_utils.scroll_utils.ItemsPositionGetter;
import com.volokh.list_visibility_utils.scroll_utils.ListViewItemPositionGetter;
import com.volokh.list_visibility_utils.view.ListView;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VisibilityUtilsFraction extends Fraction implements VisibilityItem.ItemCallback {


    public interface VisibilityUtilsCallback {
        void setTitle(String title);
    }

    private VisibilityUtilsCallback mVisibilityUtilsCallback = new VisibilityUtilsCallback() {
        @Override
        public void setTitle(String title) {

        }
    };
    private ToastDialog mToast;
    private int mScrollState;
    private ListView mListContainer;
    private ItemsPositionGetter mItemsPositionGetter;
    private Component root;
    private boolean isFirst = false;

    private List<VisibilityItem> mList = new ArrayList<>(Arrays.asList(
            new VisibilityItem("1", this),
            new VisibilityItem("2", this),
            new VisibilityItem("3", this),
            new VisibilityItem("4", this),
            new VisibilityItem("5", this),
            new VisibilityItem("6", this),
            new VisibilityItem("7", this),
            new VisibilityItem("8", this),
            new VisibilityItem("9", this),
            new VisibilityItem("10", this)));

    private final ListItemsVisibilityCalculator mListItemVisibilityCalculator =
            new SingleListViewItemActiveCalculator(new DefaultSingleItemCalculatorCallback(), mList);


    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        root = scatter.parse(ResourceTable.Layout_visibility_utils_fraction, container, false);

        mListContainer = new ListView(getContext());
        DependentLayout.LayoutConfig layoutConfig = new DependentLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        DependentLayout dependentLayout = (DependentLayout) root.findComponentById(ResourceTable.Id_visibility_main);
        dependentLayout.addComponent(mListContainer, layoutConfig);

        VisibilityUtilsAdapter adapter = new VisibilityUtilsAdapter(mList);

        mListContainer.setItemProvider(adapter);
        mItemsPositionGetter = new ListViewItemPositionGetter(mListContainer);

        mListContainer.setScrolledListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(ListContainer view, int scrollState) {
                mScrollState = scrollState;
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && !mList.isEmpty()) {

                    mListItemVisibilityCalculator.onScrollStateIdle(
                            mItemsPositionGetter,
                            view.getFirstVisibleItemPosition(),
                            view.getLastVisibleItemPosition());
                }
            }

            @Override
            public void onScroll(ListContainer view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (!isFirst) {
                    isFirst = true;
                    mListItemVisibilityCalculator.onScrollStateIdle(
                            mItemsPositionGetter,
                            view.getFirstVisibleItemPosition(),
                            view.getLastVisibleItemPosition());
                }
                if (!mList.isEmpty() && isFirst) {
                    mListItemVisibilityCalculator.onScroll(
                            mItemsPositionGetter,
                            view.getFirstVisibleItemPosition(),
                            view.getLastVisibleItemPosition() - view.getFirstVisibleItemPosition() + 1,
                            mScrollState);
                }
            }
        });

        return root;
    }

    @Override
    public Component getComponent() {
        return root;
    }

    @Override
    public void makeToast(String text) {
        if (mToast != null) {
            mToast.cancel();
            mToast = new ToastDialog(this);
            mToast.setText(text);
            mToast.show();
        }
    }

    @Override
    public void onActiveViewChangedActive(Component newActiveView, int newActiveViewPosition) {
        mVisibilityUtilsCallback.setTitle("Active view at position " + newActiveViewPosition);
    }


}

