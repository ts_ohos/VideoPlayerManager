package com.ohos.vpmanager.visibility_demo.adapter.items;

import com.ohos.vpmanager.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

public class Holder {

  public Text position;

  public Holder(Component itemView){
      position = (Text) itemView.findComponentById(ResourceTable.Id_position);
  }



}
