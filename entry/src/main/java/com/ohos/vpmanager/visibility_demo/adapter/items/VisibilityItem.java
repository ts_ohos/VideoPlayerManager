package com.ohos.vpmanager.visibility_demo.adapter.items;

import com.ohos.vpmanager.ResourceTable;
import com.ohos.vpmanager.utils.ApplicationContext;
import com.volokh.list_visibility_utils.items.ListItem;
import com.volokh.list_visibility_utils.utils.Config;
import com.volokh.list_visibility_utils.utils.Logger;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.utils.Rect;

public class VisibilityItem implements ListItem {

    private static final boolean SHOW_LOGS = Config.SHOW_LOGS;
    private static final String TAG = VisibilityItem.class.getSimpleName();

    private final String mTitle;
    private final Rect mCurrentViewRect = new Rect();
    private final ItemCallback mItemCallback;


    public interface ItemCallback {
        void makeToast(String text);

        void onActiveViewChangedActive(Component newActiveView, int newActiveViewPosition);
    }

    public VisibilityItem(String title, ItemCallback callback) {
        mTitle = title;
        mItemCallback = callback;
    }


    @Override
    public int getVisibilityPercents(Component view) {
        if (SHOW_LOGS) Logger.v(TAG, ">> getVisibilityPercents view " + view);

        int percents = 100;

        view.getSelfVisibleRect(mCurrentViewRect);
        if (SHOW_LOGS)
            Logger.v(TAG, "getVisibilityPercents mCurrentViewRect top " + mCurrentViewRect.top + ", left " + mCurrentViewRect.left + ", bottom " + mCurrentViewRect.bottom + ", right " + mCurrentViewRect.right);

        int height = view.getHeight();
        if (SHOW_LOGS) Logger.v(TAG, "getVisibilityPercents height " + height);

        if (viewIsPartiallyHiddenTop()) {
            // view is partially hidden behind the top edge
            percents = (height - mCurrentViewRect.top) * 100 / height;
        } else if (viewIsPartiallyHiddenBottom(height)) {
            percents = mCurrentViewRect.bottom * 100 / height;
        }

        if (SHOW_LOGS) Logger.v(TAG, "<< getVisibilityPercents, percents " + percents);

        return percents;
    }

    @Override
    public void setActive(Component newActiveView, int newActiveViewPosition) {
        if (SHOW_LOGS) Logger.v(TAG, "setActive, newActiveViewPosition " + newActiveViewPosition);


            Component whiteView = newActiveView.findComponentById(ResourceTable.Id_white_view);
            // animate alpha to show that active view has changed
            AnimatorProperty animatorProperty = whiteView.createAnimatorProperty();
            animatorProperty.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {

                }

                @Override
                public void onStop(Animator animator) {

                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animator) {
                    ApplicationContext.getAbilitySlice().getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
                        @Override
                        public void run() {
                            AnimatorProperty animatorProperty = whiteView.createAnimatorProperty();
                                animatorProperty.alpha(0f)
                                        .setDuration(500)
                                        .start();
                        }
                    });
                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            });

            animatorProperty.alpha(1f)
                    .setDuration(500)
                    .start();


            mItemCallback.makeToast("New Active View at position " + newActiveViewPosition);
            mItemCallback.onActiveViewChangedActive(newActiveView, newActiveViewPosition);
    }

    @Override
    public void deactivate(Component currentView, int position) {

    }

    private boolean viewIsPartiallyHiddenBottom(int height) {
        return mCurrentViewRect.bottom > 0 && mCurrentViewRect.bottom < height;
    }

    private boolean viewIsPartiallyHiddenTop() {
        return mCurrentViewRect.top > 0;
    }
}
