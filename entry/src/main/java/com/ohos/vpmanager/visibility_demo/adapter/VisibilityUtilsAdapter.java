package com.ohos.vpmanager.visibility_demo.adapter;

import com.ohos.vpmanager.ResourceTable;
import com.ohos.vpmanager.utils.ApplicationContext;
import com.ohos.vpmanager.visibility_demo.adapter.items.Holder;
import com.ohos.vpmanager.visibility_demo.adapter.items.VisibilityItem;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.RecycleItemProvider;

import java.util.List;

public class VisibilityUtilsAdapter extends RecycleItemProvider {

    private final List<VisibilityItem> mList;


    public VisibilityUtilsAdapter(List<VisibilityItem> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component component1 = LayoutScatter.getInstance(ApplicationContext.getAbilitySlice().getContext())
                .parse(ResourceTable.Layout_visibility_adapter_item, null, false);
        Holder holder = new Holder(component1);
        holder.position.setText("Position :" + i);

        return component1;
    }

}
