/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ohos.vpmanager.utils;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class LogUtils {
    private static HiLogLabel hiLogLabel;

    public static int error(String TAG, final String message) {
        hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x00201, TAG);
        return HiLog.error(hiLogLabel, message);
    }

    public static int warn(String TAG, final String message) {
        hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x00201, TAG);
        return HiLog.warn(hiLogLabel, message);
    }

    public static int info(String TAG, final String message) {
        hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x00201, TAG);
        return HiLog.info(hiLogLabel, message);
    }

    public static int debug(String TAG, final String message) {
        hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x00201, TAG);
        return HiLog.debug(hiLogLabel, message);
    }

}
