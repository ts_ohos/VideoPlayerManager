/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ohos.vpmanager.slice;

import com.ohos.vpmanager.ResourceTable;
import com.ohos.vpmanager.utils.ApplicationContext;
import com.ohos.vpmanager.video_list_demo.fraction.VideoListViewFraction;
import com.ohos.vpmanager.video_player_manager_demo.fraction.VideoPlayerManagerFraction;
import com.ohos.vpmanager.visibility_demo.fraction.VisibilityUtilsFraction;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;

public class MainAbilitySlice extends AbilitySlice {

    private DirectionalLayout bar;

    private Component vpBar;

    private Image menu;

    private CommonDialog commonDialog = new CommonDialog(this);

    //Fragment
    private Fraction videoListView = new VideoListViewFraction();

    private DirectionalLayout ListView;
    private DirectionalLayout Visibility;
    private DirectionalLayout Player;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        ApplicationContext.setAbilitySlice(this);
        initView();
        clickListener();
    }

    private void initView() {
        bar = (DirectionalLayout) findComponentById(ResourceTable.Id_bar);
        vpBar = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_bar, null, false);
        bar.addComponent(vpBar);

        menu = (Image) findComponentById(ResourceTable.Id_menu);

        ((FractionAbility) getAbility()).getFractionManager().startFractionScheduler()
                .add(ResourceTable.Id_fragment_container,
                        videoListView).submit();

    }

    private void clickListener() {
        menu.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!commonDialog.isShowing()) {
                    popWindows();
                }
            }
        });
    }

    private void popWindows() {
        Component component = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_commondialog, null, true);
        commonDialog.setContentCustomComponent(component);
        commonDialog.setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        commonDialog.setAlignment(LayoutAlignment.RIGHT | LayoutAlignment.TOP);
        commonDialog.setOffset(0, 330);
        commonDialog.setSwipeToDismiss(true);
        commonDialog.siteRemovable(true);
        commonDialog.setAutoClosable(true);
        commonDialog.show();
        ListView = (DirectionalLayout) component.findComponentById(ResourceTable.Id_ListView);
        Visibility = (DirectionalLayout) component.findComponentById(ResourceTable.Id_Visibility);
        Player = (DirectionalLayout) component.findComponentById(ResourceTable.Id_Player);

        ListView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                addListView();
                commonDialog.remove();
            }
        });
        Visibility.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                addVisibilityUtilsFragment();
                commonDialog.remove();
            }
        });
        Player.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                addVideoPlayerManagerFragment();
                commonDialog.remove();
            }
        });
    }

    private void addVideoPlayerManagerFragment() {
        ((FractionAbility) getAbility()).getFractionManager().startFractionScheduler().replace(ResourceTable.Id_fragment_container, new VideoPlayerManagerFraction()).submit();
    }

    private void addVisibilityUtilsFragment() {
        ((FractionAbility) getAbility()).getFractionManager().startFractionScheduler().replace(ResourceTable.Id_fragment_container, new VisibilityUtilsFraction()).submit();
    }


    private void addListView() {
        ((FractionAbility) getAbility()).getFractionManager().startFractionScheduler().replace(ResourceTable.Id_fragment_container, new VideoListViewFraction()).submit();
    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onBackground() {
        terminate();
        super.onBackground();
    }
}
