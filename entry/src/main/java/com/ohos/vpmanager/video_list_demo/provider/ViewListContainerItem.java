package com.ohos.vpmanager.video_list_demo.provider;

import com.ohos.vpmanager.utils.LogUtils;
import com.ohos.vpmanager.video_list_demo.holders.VideoViewHolder;
import com.ohos.vpmanager.video_list_demo.items.BaseVideoItem;
import com.volokh.video_player_manager.manager.VideoPlayerManager;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.RecycleItemProvider;
import ohos.app.Context;

import java.util.List;

public class ViewListContainerItem extends RecycleItemProvider {

    private static final String TAG = "ViewListContainerItem";
    private final Context mContext;
    private final VideoPlayerManager mVideoPlayerManager;
    private final List<BaseVideoItem> mList;

    public ViewListContainerItem(VideoPlayerManager videoPlayerManager, Context context, List<BaseVideoItem> list) {
        mVideoPlayerManager = videoPlayerManager;
        mContext = context;
        mList = list;
        LogUtils.info(TAG, "list size = " + list.size());
    }


    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer componentContainer) {
        LogUtils.info(TAG, "position = " + position);
        BaseVideoItem videoItem = mList.get(position);
        Component resultView;
        if (convertView == null) {
            resultView = videoItem.createView(componentContainer);
        } else {
            resultView = convertView;
        }
        LogUtils.info(TAG, "resultView.getTag = " + resultView.getTag());
        videoItem.update(position, (VideoViewHolder) resultView.getTag(), mVideoPlayerManager);
        return resultView;
    }


    @Override
    public void notifyDataSetItemRemoved(int position) {
        super.notifyDataSetItemRemoved(position);
    }

}
