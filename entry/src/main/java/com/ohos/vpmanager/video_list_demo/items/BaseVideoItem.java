package com.ohos.vpmanager.video_list_demo.items;


import com.ohos.vpmanager.ResourceTable;
import com.ohos.vpmanager.utils.ApplicationContext;
import com.ohos.vpmanager.utils.LogUtils;
import com.ohos.vpmanager.video_list_demo.holders.VideoViewHolder;
import com.volokh.list_visibility_utils.items.ListItem;
import com.volokh.list_visibility_utils.utils.Logger;
import com.volokh.video_player_manager.manager.VideoItem;
import com.volokh.video_player_manager.manager.VideoPlayerManager;
import com.volokh.video_player_manager.meta.CurrentItemMetaData;
import com.volokh.video_player_manager.meta.MetaData;
import com.volokh.video_player_manager.ui.MediaPlayerWrapper;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.Rect;

public abstract class BaseVideoItem implements VideoItem, ListItem {

    private static final boolean SHOW_LOGS = false;
    private static final String TAG = BaseVideoItem.class.getSimpleName();

    /**
     * An object that is filled with values when {@link #getVisibilityPercents} method is called.
     * This object is local visible rect filled by {@link Component#getSelfVisibleRect(Rect)}
     */

    private final Rect mCurrentViewRect = new Rect();
    private final VideoPlayerManager<MetaData> mVideoPlayerManager;

    protected BaseVideoItem(VideoPlayerManager<MetaData> videoPlayerManager) {
        mVideoPlayerManager = videoPlayerManager;
    }

    /**
     * This method needs to be called when created/recycled view is updated.
     * Call it in
     * 1. {@link ohos.agp.components.RecycleItemProvider#getComponent(int, Component, ComponentContainer)} (int, View, ViewGroup)}
     */
    public abstract void update(int position, VideoViewHolder view, VideoPlayerManager videoPlayerManager);

    /**
     * When this item becomes active we start playback on the video in this item
     */
    @Override
    public void setActive(Component newActiveView, int newActiveViewPosition) {
        VideoViewHolder viewHolder = (VideoViewHolder) newActiveView.getTag();
        LogUtils.info(TAG, "setActive = " + viewHolder.mPlayer + "Manager = " + mVideoPlayerManager);
        playNewVideo(new CurrentItemMetaData(newActiveViewPosition, newActiveView), viewHolder.mPlayer, mVideoPlayerManager);
    }

    /**
     * When this item becomes inactive we stop playback on the video in this item.
     */
    @Override
    public void deactivate(Component currentView, int position) {
        stopPlayback(mVideoPlayerManager);
    }

    public Component createView(ComponentContainer parent) {

        Component view = LayoutScatter.getInstance(ApplicationContext.getAbilitySlice()).parse(ResourceTable.Layout_video_item, null, false);
        LogUtils.info(TAG, "createView  view = " + view);

        final VideoViewHolder videoViewHolder = new VideoViewHolder(view.getContext(), view);

        view.setTag(videoViewHolder);

        videoViewHolder.mPlayer.addMediaPlayerListener(new MediaPlayerWrapper.MainThreadMediaPlayerListener() {
            @Override
            public void onVideoSizeChangedMainThread(int width, int height) {
                LogUtils.info(TAG, "onVideoSizeChangedMainThread");
            }

            @Override
            public void onVideoPreparedMainThread() {
                LogUtils.info(TAG, "onVideoPreparedMainThread");
                videoViewHolder.mCover.setVisibility(Component.HIDE);
            }

            @Override
            public void onVideoCompletionMainThread() {
                LogUtils.info(TAG, "onVideoCompletionMainThread");
            }

            @Override
            public void onErrorMainThread(int what, int extra) {
                LogUtils.info(TAG, "onErrorMainThread");
            }

            @Override
            public void onBufferingUpdateMainThread(int percent) {
                LogUtils.info(TAG, "onBufferingUpdateMainThread");
            }

            @Override
            public void onVideoStoppedMainThread() {
                LogUtils.info(TAG, "onVideoStoppedMainThread");
                videoViewHolder.mCover.setVisibility(Component.VISIBLE);
            }
        });
        return view;
    }

    /**
     * This method calculates visibility percentage of currentView.
     * This method works correctly when currentView is smaller then it's enclosure.
     *
     * @param currentView - view which visibility should be calculated
     * @return currentView visibility percents
     */
    @Override
    public int getVisibilityPercents(Component currentView) {
        if (SHOW_LOGS) Logger.v(TAG, ">> getVisibilityPercents currentView " + currentView);

        int percents = 100;

        currentView.getSelfVisibleRect(mCurrentViewRect);
        if (SHOW_LOGS)
            Logger.v(TAG, "getVisibilityPercents mCurrentViewRect top " + mCurrentViewRect.top + ", left " + mCurrentViewRect.left + ", bottom " + mCurrentViewRect.bottom + ", right " + mCurrentViewRect.right);

        int height = currentView.getHeight();
        if (SHOW_LOGS) Logger.v(TAG, "getVisibilityPercents height " + height);

        if (viewIsPartiallyHiddenTop()) {
            // view is partially hidden behind the top edge
            percents = (height - mCurrentViewRect.top) * 100 / height;
        } else if (viewIsPartiallyHiddenBottom(height)) {
            percents = mCurrentViewRect.bottom * 100 / height;
        }

        setVisibilityPercentsText(currentView, percents);
        if (SHOW_LOGS) Logger.v(TAG, "<< getVisibilityPercents, percents " + percents);

        return percents;
    }

    private void setVisibilityPercentsText(Component currentView, int percents) {
        if (SHOW_LOGS) Logger.v(TAG, "setVisibilityPercentsText percents " + percents);
        VideoViewHolder videoViewHolder = (VideoViewHolder) currentView.getTag();
        String percentsText = "Visibility percents: " + String.valueOf(percents);
        ApplicationContext.getAbilitySlice().getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
            @Override
            public void run() {
                videoViewHolder.mVisibilityPercents.setText(percentsText);
            }
        });

    }

    private boolean viewIsPartiallyHiddenBottom(int height) {
        return mCurrentViewRect.bottom > 0 && mCurrentViewRect.bottom < height;
    }

    private boolean viewIsPartiallyHiddenTop() {
        return mCurrentViewRect.top > 0;
    }
}
