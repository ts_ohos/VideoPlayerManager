package com.ohos.vpmanager.video_list_demo.holders;

import com.ohos.vpmanager.ResourceTable;
import com.volokh.video_player_manager.ui.VideoPlayerView;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.app.Context;


public class VideoViewHolder {

    public final VideoPlayerView mPlayer;
    public final Text mTitle;
    public final Image mCover;
    public final Text mVisibilityPercents;
    public final DependentLayout mMediaPlayerLayout;

    public VideoViewHolder(Context context, Component view) {
        mPlayer = new VideoPlayerView(context);
        mMediaPlayerLayout = (DependentLayout) view.findComponentById(ResourceTable.Id_player);
        mMediaPlayerLayout.addComponent(mPlayer);
        mTitle = (Text) view.findComponentById(ResourceTable.Id_title);
        mCover = (Image) view.findComponentById(ResourceTable.Id_cover);
        mVisibilityPercents = (Text) view.findComponentById(ResourceTable.Id_visibility_percents);
    }


    @Override
    public String toString() {
        return super.toString();
    }
}
