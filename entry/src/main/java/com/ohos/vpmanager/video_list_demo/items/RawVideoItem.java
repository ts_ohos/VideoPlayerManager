package com.ohos.vpmanager.video_list_demo.items;

import com.ohos.vpmanager.utils.LogUtils;
import com.ohos.vpmanager.video_list_demo.holders.VideoViewHolder;
import com.volokh.video_player_manager.Config;
import com.volokh.video_player_manager.manager.VideoPlayerManager;
import com.volokh.video_player_manager.meta.MetaData;
import com.volokh.video_player_manager.ui.VideoPlayerView;
import ohos.agp.components.Component;
import ohos.global.resource.RawFileDescriptor;


public class RawVideoItem extends BaseVideoItem {

    private static final String TAG = RawVideoItem.class.getSimpleName();
    private static final boolean SHOW_LOGS = Config.SHOW_LOGS;

    private final RawFileDescriptor mAssetFileDescriptor;
    private final String mTitle;

    private final int mImageResource;
    VideoPlayerManager<MetaData> mVideoPlayerManager;

    public RawVideoItem(String title, RawFileDescriptor assetFileDescriptor, VideoPlayerManager<MetaData> videoPlayerManager, int imageResource) {
        super(videoPlayerManager);
        mTitle = title;
        mAssetFileDescriptor = assetFileDescriptor;
        mImageResource = imageResource;
        mVideoPlayerManager = videoPlayerManager;
    }

    @Override
    public void update(int position, final VideoViewHolder viewHolder, VideoPlayerManager videoPlayerManager) {
        LogUtils.info(TAG, "position = " + position);
        viewHolder.mTitle.setText(mTitle);
        viewHolder.mCover.setVisibility(Component.VISIBLE);
        viewHolder.mCover.setPixelMap(mImageResource);
    }

    @Override
    public void playNewVideo(MetaData currentItemMetaData, VideoPlayerView player, VideoPlayerManager<MetaData> videoPlayerManager) {
        LogUtils.info(TAG, "playNewVideo  player =  " + player + " currentItemMetaData = " + currentItemMetaData);
        videoPlayerManager.playNewVideo(currentItemMetaData, player, mAssetFileDescriptor);
    }

    @Override
    public void stopPlayback(VideoPlayerManager videoPlayerManager) {
        videoPlayerManager.stopAnyPlayback();
    }

    @Override
    public String toString() {
        return getClass() + ", mTitle[" + mTitle + "]";
    }
}
