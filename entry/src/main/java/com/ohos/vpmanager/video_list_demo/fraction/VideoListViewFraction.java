package com.ohos.vpmanager.video_list_demo.fraction;


import com.ohos.vpmanager.ResourceTable;
import com.ohos.vpmanager.utils.ApplicationContext;
import com.ohos.vpmanager.utils.LogUtils;
import com.ohos.vpmanager.video_list_demo.items.BaseVideoItem;
import com.ohos.vpmanager.video_list_demo.items.ItemFactory;
import com.ohos.vpmanager.video_list_demo.provider.ViewListContainerItem;
import com.volokh.list_visibility_utils.calculator.AbsListView;
import com.volokh.list_visibility_utils.calculator.DefaultSingleItemCalculatorCallback;
import com.volokh.list_visibility_utils.calculator.ListItemsVisibilityCalculator;
import com.volokh.list_visibility_utils.calculator.SingleListViewItemActiveCalculator;
import com.volokh.list_visibility_utils.scroll_utils.ItemsPositionGetter;
import com.volokh.list_visibility_utils.scroll_utils.ListViewItemPositionGetter;
import com.volokh.list_visibility_utils.view.ListView;
import com.volokh.video_player_manager.Config;
import com.volokh.video_player_manager.manager.PlayerItemChangeListener;
import com.volokh.video_player_manager.manager.SingleVideoPlayerManager;
import com.volokh.video_player_manager.manager.VideoPlayerManager;
import com.volokh.video_player_manager.meta.MetaData;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;

import java.io.IOException;
import java.util.ArrayList;

/**
 * This Fraction shows of how to use {@link VideoPlayerManager} with a ListView.
 */
public class VideoListViewFraction extends Fraction {

    private static final String TAG = VideoListViewFraction.class.getSimpleName();

    private static final boolean SHOW_LOGS = Config.SHOW_LOGS;

    private ListView mListView;

    private int mScrollState = AbsListView.OnScrollListener.SCROLL_STATE_IDLE;

    private final ArrayList<BaseVideoItem> mList = new ArrayList<>();
    /**
     * Only the one (most visible) view should be active (and playing).
     * To calculate visibility of views we use {@link SingleListViewItemActiveCalculator}
     */
    private final ListItemsVisibilityCalculator mListItemVisibilityCalculator =
            new SingleListViewItemActiveCalculator(new DefaultSingleItemCalculatorCallback(), mList);
    /**
     * items position in the ListView
     */
    private ItemsPositionGetter mItemsPositionGetter;
    /**
     * Here we use {@link SingleVideoPlayerManager}, which means that only one video playback is possible.
     */
    private final VideoPlayerManager<MetaData> mVideoPlayerManager = new SingleVideoPlayerManager(new PlayerItemChangeListener() {
        @Override
        public void onPlayerItemChanged(MetaData metaData) {
            if (SHOW_LOGS) LogUtils.info(TAG, "onPlayerItemChanged " + metaData);
        }
    });
    private Component rootView;

    private Boolean isFirst = false;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager windowManager = WindowManager.getInstance();
        Window window = windowManager.getTopWindow().get();
        window.setTransparent(true);
        LogUtils.info(TAG + "onStart", "onStart");
    }


    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        LogUtils.info(TAG + "onComponentAttached", "start");
        initViewList();
        rootView = scatter.parse(ResourceTable.Layout_layout, container, false);
        mListView = new ListView(getContext());
        DependentLayout.LayoutConfig layoutConfig = new DependentLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        DependentLayout dependentLayout = (DependentLayout) rootView.findComponentById(ResourceTable.Id_main_layout);
        dependentLayout.addComponent(mListView, layoutConfig);

        ViewListContainerItem viewListContainerItem = new ViewListContainerItem(mVideoPlayerManager, ApplicationContext.getAbilitySlice(), mList);

        mListView.setItemProvider(viewListContainerItem);

        mItemsPositionGetter = new ListViewItemPositionGetter(mListView);
        mListView.setLayoutRefreshedListener(new Component.LayoutRefreshedListener() {
            @Override
            public void onRefreshed(Component component) {
                // need to call this method from list view handler in order to have filled list
                if (!isFirst) {
                    isFirst = true;
                    mListItemVisibilityCalculator.onScrollStateIdle(
                            mItemsPositionGetter,
                            mListView.getFirstVisibleItemPosition(),
                            mListView.getLastVisibleItemPosition());
                }
            }
        });

        mListView.setScrolledListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(ListContainer view, int scrollState) {
                mScrollState = scrollState;
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && !mList.isEmpty()) {
                    mListItemVisibilityCalculator.onScrollStateIdle(mItemsPositionGetter,
                            view.getFirstVisibleItemPosition(),
                            view.getLastVisibleItemPosition());
                }
            }

            @Override
            public void onScroll(ListContainer view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (!mList.isEmpty()&& isFirst) {
                    mListItemVisibilityCalculator.onScroll(mItemsPositionGetter, firstVisibleItem, visibleItemCount, mScrollState);
                }

            }
        });
        return rootView;
    }


    @Override
    public Component getComponent() {
        return rootView;
    }

    private Context getActivity() {
        return ApplicationContext.getAbilitySlice();
    }

    // 初始化list
    private void initViewList() {
        try {
            mList.add(ItemFactory.createItemFromAsset("resources/rawfile/video_sample_1.mp4", ResourceTable.Media_video_sample_1_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("resources/rawfile/video_sample_2.mp4", ResourceTable.Media_video_sample_2_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("resources/rawfile/video_sample_3.mp4", ResourceTable.Media_video_sample_3_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("resources/rawfile/video_sample_4.mp4", ResourceTable.Media_video_sample_4_pic, getActivity(), mVideoPlayerManager));

            mList.add(ItemFactory.createItemFromAsset("resources/rawfile/video_sample_1.mp4", ResourceTable.Media_video_sample_1_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("resources/rawfile/video_sample_2.mp4", ResourceTable.Media_video_sample_2_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("resources/rawfile/video_sample_3.mp4", ResourceTable.Media_video_sample_3_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("resources/rawfile/video_sample_4.mp4", ResourceTable.Media_video_sample_4_pic, getActivity(), mVideoPlayerManager));

            mList.add(ItemFactory.createItemFromAsset("resources/rawfile/video_sample_1.mp4", ResourceTable.Media_video_sample_1_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("resources/rawfile/video_sample_2.mp4", ResourceTable.Media_video_sample_2_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("resources/rawfile/video_sample_3.mp4", ResourceTable.Media_video_sample_3_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("resources/rawfile/video_sample_4.mp4", ResourceTable.Media_video_sample_4_pic, getActivity(), mVideoPlayerManager));

        } catch (IOException e) {
            LogUtils.error(TAG, e.toString());
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mVideoPlayerManager.resetMediaPlayer();
    }

    @Override
    protected void onActive() {
        super.onActive();
        LogUtils.info(TAG, "onActive ");
    }
}
