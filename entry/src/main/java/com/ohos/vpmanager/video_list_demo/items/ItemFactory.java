package com.ohos.vpmanager.video_list_demo.items;


import com.volokh.video_player_manager.manager.VideoPlayerManager;
import com.volokh.video_player_manager.meta.MetaData;
import ohos.app.Context;

import java.io.IOException;

public class ItemFactory {

    public static BaseVideoItem createItemFromAsset(String rawName, int imageResource, Context activity, VideoPlayerManager<MetaData> videoPlayerManager) throws IOException {
        return new RawVideoItem(rawName, activity.getResourceManager().getRawFileEntry(rawName).openRawFileDescriptor(), videoPlayerManager, imageResource);
    }
}
