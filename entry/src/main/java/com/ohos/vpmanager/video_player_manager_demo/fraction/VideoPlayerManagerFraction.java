package com.ohos.vpmanager.video_player_manager_demo.fraction;

import com.ohos.vpmanager.ResourceTable;
import com.ohos.vpmanager.utils.ApplicationContext;
import com.volokh.video_player_manager.manager.PlayerItemChangeListener;
import com.volokh.video_player_manager.manager.SingleVideoPlayerManager;
import com.volokh.video_player_manager.manager.VideoPlayerManager;
import com.volokh.video_player_manager.meta.MetaData;
import com.volokh.video_player_manager.ui.SimpleMainThreadMediaPlayerListener;
import com.volokh.video_player_manager.ui.VideoPlayerView;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.global.resource.RawFileDescriptor;

import java.io.IOException;

import static ohos.agp.components.Component.INVISIBLE;
import static ohos.agp.components.Component.VISIBLE;

public class VideoPlayerManagerFraction extends Fraction implements Component.ClickedListener {


    private VideoPlayerView mVideoPlayer_1;
    private VideoPlayerView mVideoPlayer_2;

    private VideoPlayerManager<MetaData> mVideoPlayerManager = new SingleVideoPlayerManager(new PlayerItemChangeListener() {
        @Override
        public void onPlayerItemChanged(MetaData metaData) {

        }
    });

    private RawFileDescriptor mVideoFileDecriptor_sample_1;
    private RawFileDescriptor mVideoFileDecriptor_sample_2;

    private Image mVideoCover;
    private Image mVideoCover2;


    private Component component;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {

        component = scatter.parse(ResourceTable.Layout_video_player_manager_fraction, container, false);
        try {
            mVideoFileDecriptor_sample_1 = ApplicationContext.getAbilitySlice().getContext().getResourceManager().getRawFileEntry("resources/rawfile/video_sample_1.mp4").openRawFileDescriptor();
            mVideoFileDecriptor_sample_2 = ApplicationContext.getAbilitySlice().getContext().getResourceManager().getRawFileEntry("resources/rawfile/video_sample_2.mp4").openRawFileDescriptor();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mVideoPlayer_1 = (VideoPlayerView) component.findComponentById(ResourceTable.Id_video_player_1);
        mVideoPlayer_1.addMediaPlayerListener(new SimpleMainThreadMediaPlayerListener() {


            @Override
            public void onVideoPreparedMainThread() {
                mVideoCover.setVisibility(INVISIBLE);
            }

            @Override
            public void onVideoStoppedMainThread() {
                mVideoCover.setVisibility(VISIBLE);
            }

            @Override
            public void onVideoCompletionMainThread() {
                ApplicationContext.getAbilitySlice().getMainTaskDispatcher().asyncDispatch(new Runnable() {
                    @Override
                    public void run() {
                        mVideoCover.setVisibility(VISIBLE);
                    }
                });
            }

        });

        mVideoCover = (Image) component.findComponentById(ResourceTable.Id_video_cover_1);
        mVideoCover.setClickedListener(this);

        mVideoPlayer_2 = (VideoPlayerView) component.findComponentById(ResourceTable.Id_video_player_2);
        mVideoPlayer_2.addMediaPlayerListener(new SimpleMainThreadMediaPlayerListener() {
            @Override
            public void onVideoPreparedMainThread() {
                mVideoCover2.setVisibility(INVISIBLE);
            }

            @Override
            public void onVideoStoppedMainThread() {
                mVideoCover2.setVisibility(VISIBLE);
            }

            @Override
            public void onVideoCompletionMainThread() {

                ApplicationContext.getAbilitySlice().getMainTaskDispatcher().asyncDispatch(new Runnable() {
                    @Override
                    public void run() {
                        mVideoCover2.setVisibility(VISIBLE);
                    }
                });
            }
        });
        mVideoCover2 = (Image) component.findComponentById(ResourceTable.Id_video_cover_2);
        mVideoCover2.setClickedListener(this);

        mVideoCover.setPixelMap(ResourceTable.Media_video_sample_1_pic);
        mVideoCover2.setPixelMap(ResourceTable.Media_video_sample_2_pic);
        return component;
    }

    @Override
    public Component getComponent() {
        return component;
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_video_cover_1:
                mVideoPlayerManager.playNewVideo(null, mVideoPlayer_1, mVideoFileDecriptor_sample_1);
                break;
            case ResourceTable.Id_video_cover_2:
                mVideoPlayerManager.playNewVideo(null, mVideoPlayer_2, mVideoFileDecriptor_sample_2);
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mVideoCover.setVisibility(VISIBLE);
        mVideoCover2.setVisibility(VISIBLE);
        mVideoPlayerManager.stopAnyPlayback();
    }
}
