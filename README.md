# VideoPlayerManager

#### 介绍
harmonyos实现的VideoPlayerManager功能

Android项目源地址：https://github.com/danylovolokh/VideoPlayerManager

移植版本：Branches/master

这是一个旨在帮助控制player类的项目。在Listcontainer中使用播放器更容易。

1. video_player_manager包提供了在后台线程中调用Player播放器方法的功能。
当列表中有多个媒体文件时，它有一个只能播放一次的实用程序。在开始新的播放之前，它将停止旧的播放并释放所有资源。
2. List_visibility_utils包，这是一个库，用于跟踪列表中最可见的项并在它们更改时通知。

注意：应该有一个最明显的项目。如果有三个或更多的项目具有相同的可见性百分比，则结果可能无法预测。  
建议您不要在屏幕上看到任何视图，或者视图足够大，以便只有一个视图是最可见的。

#### 实现功能
1.视频播放；
2.图片显示隐藏；
3.滑动显示item百分比；
4.根据百分比切换图片和视频；
5.实现3个效果demo。

#### 安装教程

方式一：

通过library生成har包,添加har包到集成的libs文件夹内
在entry的gradle内添加如下代码
```
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```
方式二:
Mavn库 下载使用
```
buildscript {
    repositories {
        ...
        mavenCentral()
    }
 allprojects {
     repositories {
         ...
         mavenCentral()
     }
 }
```
```
  implementation 'com.gitee.ts_ohos:VideoPlayerManager_list_visibility:1.0.1'
  implementation 'com.gitee.ts_ohos:VideoPlayerManager_player_manager:1.0.1'
```

#### 使用说明

功能具体实现请根据 demo （entry）部分实现构建  
1.Video_List_demo 实现ListContainer下的滑动根据显示百分比来切换视频和图片的效果  
具体的操作滑动事件的监听：
```
mListView.setScrolledListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(ListContainer view, int scrollState) {
                mScrollState = scrollState;
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && !mList.isEmpty()) {
                    mListItemVisibilityCalculator.onScrollStateIdle(mItemsPositionGetter,
                            view.getFirstVisibleItemPosition(),
                            view.getLastVisibleItemPosition());
                }
            }

            @Override
            public void onScroll(ListContainer view,
                                 int firstVisibleItem,
                                 int visibleItemCount,
                                 int totalItemCount) {
                if (!mList.isEmpty()&& isFirst) {
                    mListItemVisibilityCalculator.onScroll(mItemsPositionGetter,
                     firstVisibleItem,
                     visibleItemCount,
                     mScrollState);
                }

            }
        });
```  
![ListContainer滑动操作页面](https://images.gitee.com/uploads/images/2021/0415/101818_95c9dac8_8562620.png "videoList.png")  
2.Video_player_manager_demo 实现视频图片切换效果  
![视频图片切换](https://images.gitee.com/uploads/images/2021/0415/101903_7e959fb6_8562620.png "videoPlayerManager.png")  
3.visibility_demo 实现的是图片根据显示百分比来显示隐藏监听的效果  
![滑动来控制item页面](https://images.gitee.com/uploads/images/2021/0415/101928_44bf184b_8562620.png "listVisibilityUtils.png")

#### 移植差异

1. Widgets各类小部件，RecyclerView页面中harmonyos的ListContainer没有拖拽接口，无法实现拖拽功能；
2. FlowLayout harmonyos没有对应接口，无法实现；


#### 版本迭代
v1.0.0 基于原项目最新版本，初次提交。

#### 源项目许可证

Copyright 2015 Danylo Volokh

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file

except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the

License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,

either express or implied. See the License for the specific language governing permissions and limitations under the License.

