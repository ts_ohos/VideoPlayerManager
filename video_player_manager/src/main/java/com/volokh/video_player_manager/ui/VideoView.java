package com.volokh.video_player_manager.ui;


import com.volokh.video_player_manager.utils.Logger;
import ohos.agp.components.surfaceprovider.SurfaceProvider;
import ohos.agp.graphics.SurfaceOps;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.global.resource.RawFileDescriptor;
import ohos.media.player.Player;

import java.io.IOException;
import java.util.Optional;

public class VideoView extends SurfaceProvider implements Player.IPlayerCallback {

    private Player player;

    private static final int MESSAGE_UPDATE_PLAY_TIME = 0;

    private EventHandler handler;

    private double screenHeight = 500.0f;
    private double screenWidth = 500.0f;

    private final String TAG = VideoView.class.getSimpleName();

    public VideoView(Context context, SurfaceOps.Callback callback) {
        super(context);
        player = new Player(getContext());
        player.setPlayerCallback(this);
        Optional surfaceHolderOptional = getSurfaceOps();

        SurfaceOps surfaceHolder = (SurfaceOps) surfaceHolderOptional.get();
        surfaceHolder.addCallback(callback);
    }

    public VideoView(Context context) {
        super(context);
        player = new Player(getContext());
        player.setPlayerCallback(this);
    }


    public void setSurfaceHolder(SurfaceOps.Callback callback) {
        Optional surfaceHolderOptional = getSurfaceOps();
        SurfaceOps surfaceHolder = (SurfaceOps) surfaceHolderOptional.get();
        surfaceHolder.addCallback(callback);
    }


    public void playAssets(String fileName, boolean isLooping, SurfaceOps holder) {
        try {
            player.setSource(getContext().getResourceManager().getRawFileEntry(fileName).openRawFileDescriptor());
            player.setVideoSurface(holder.getSurface());
            player.enableSingleLooping(isLooping);
            player.enableScreenOn(true);
            Logger.v("ViewListContainerItem  setPlayerConfig prepare = " + player.prepare(), "123");
            initPlayerViewSize();
            Logger.v("ViewListContainerItem play play = " + player.play(), "123");
        } catch (Exception e) {
            e.printStackTrace();
            Logger.v(TAG, e.toString());
        }
    }

    public void playRaw(RawFileDescriptor rawFileDescriptor, boolean isLooping, SurfaceOps holder) {
        try {
            player.setSource(rawFileDescriptor);
            player.setVideoSurface(holder.getSurface());
            player.enableSingleLooping(isLooping);
            player.enableScreenOn(true);
            Logger.v("ViewListContainerItem  setPlayerConfig prepare = " + player.prepare(), "123");
            initPlayerViewSize();
            Logger.v("ViewListContainerItem play play = " + player.play(), "123");
        } catch (Exception e) {
            e.printStackTrace();
            Logger.v(TAG, e.toString());
        }
    }

    public void setPlayerConfig(String fileName, boolean isLooping) throws IOException {
        Logger.v("ViewListContainerItem fileName = " + fileName, "1231");
        player.setSource(getContext().getResourceManager().getRawFileEntry(fileName).openRawFileDescriptor());
        player.enableSingleLooping(isLooping);
        player.enableScreenOn(true);
        Logger.v("ViewListContainerItem  setPlayerConfig prepare = " + player.prepare(), "123");
        Logger.v("ViewListContainerItem play play = " + player.play(), "123");
    }

    public boolean isPlaying() {
        return player.isNowPlaying();
    }

    public void setHandler(SurfaceOps holder) {
        player.setVideoSurface(holder.getSurface());
    }

    public void play() {
/*        LogUtils.info("ViewListContainerItem prepare play = "  + player.prepare(), "123");
        LogUtils.info("ViewListContainerItem play play = "  + player.play(), "123");*/
        //handler.sendEvent(InnerEvent.get(MESSAGE_UPDATE_PLAY_TIME));

    }

    private void initPlayerViewSize() {
        int videoWidth = player.getVideoWidth();
        int videoHeight = player.getVideoHeight();
        Logger.v(TAG, "VideoWidth = " + player.getVideoWidth() + "Height =" + player.getVideoHeight());
        if (videoWidth < videoHeight) {
            double scale = screenHeight * 1.f / videoHeight;
            double currHeight = videoHeight * scale;
            double currWidth = videoWidth * scale;
            setHeight(((int) currHeight));
            setWidth(((int) currWidth));
        } else {
            double scale = screenWidth * 1.f / videoWidth;
            double currHeight = videoHeight * scale;
            double currWidth = videoWidth * scale;
            setHeight(((int) currHeight));
            setWidth(((int) currWidth));
        }

    }


    public void stop() {
        player.stop();
    }

    public void pause() {
        player.pause();
    }


    @Override
    public void onPrepared() {

    }

    @Override
    public void onMessage(int i, int i1) {

    }

    @Override
    public void onError(int i, int i1) {

    }

    @Override
    public void onResolutionChanged(int i, int i1) {

    }

    @Override
    public void onPlayBackComplete() {

    }

    @Override
    public void onRewindToComplete() {

    }

    @Override
    public void onBufferingChange(int i) {

    }

    @Override
    public void onNewTimedMetaData(Player.MediaTimedMetaData mediaTimedMetaData) {

    }

    @Override
    public void onMediaTimeIncontinuity(Player.MediaTimeInfo mediaTimeInfo) {

    }
}
