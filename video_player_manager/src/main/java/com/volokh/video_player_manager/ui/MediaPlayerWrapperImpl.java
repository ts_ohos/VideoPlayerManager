package com.volokh.video_player_manager.ui;


import ohos.app.Context;
import ohos.media.player.Player;

public class MediaPlayerWrapperImpl extends MediaPlayerWrapper {


    public MediaPlayerWrapperImpl(Context context) {
        super(new Player(context));
        TAG = "MediaPlayerWrapperImpl";
    }
}
