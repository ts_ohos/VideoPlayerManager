package com.volokh.video_player_manager.manager;

import com.volokh.video_player_manager.meta.MetaData;

/**
 * Created by danylo.ohos on 06.01.2016.
 */
public interface PlayerItemChangeListener {
    void onPlayerItemChanged(MetaData currentItemMetaData);
}
