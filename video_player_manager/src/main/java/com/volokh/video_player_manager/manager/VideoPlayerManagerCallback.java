package com.volokh.video_player_manager.manager;

import com.volokh.video_player_manager.PlayerMessageState;
import com.volokh.video_player_manager.meta.MetaData;
import com.volokh.video_player_manager.ui.VideoPlayerView;
import com.volokh.video_player_manager.player_messages.PlayerMessage;

/**
 * This callback is used by {@link PlayerMessage}
 * to get and set data it needs
 */
public interface VideoPlayerManagerCallback {

    void setCurrentItem(MetaData currentItemMetaData, VideoPlayerView newPlayerView);

    void setVideoPlayerState(VideoPlayerView videoPlayerView, PlayerMessageState playerMessageState);

    PlayerMessageState getCurrentPlayerState();
}
