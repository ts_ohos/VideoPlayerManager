package com.volokh.video_player_manager.meta;

import ohos.agp.components.Component;

public class CurrentItemMetaData implements MetaData {

    public final int positionOfCurrentItem;
    public final Component currentItemView;

    public CurrentItemMetaData(int positionOfCurrentItem, Component currentItemView) {
        this.positionOfCurrentItem = positionOfCurrentItem;
        this.currentItemView = currentItemView;
    }

    @Override
    public String toString() {
        return "CurrentItemMetaData{"
                + "positionOfCurrentItem=" + positionOfCurrentItem
                + ", currentItemView=" + currentItemView
                + '}';
    }
}
