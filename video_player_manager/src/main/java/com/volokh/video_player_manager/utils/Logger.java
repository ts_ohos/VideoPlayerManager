package com.volokh.video_player_manager.utils;


import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class Logger {

    public static int err(final String TAG, final String message) {
        HiLogLabel hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x00201, TAG);
        return HiLog.error(hiLogLabel, TAG, message);
    }

    public static int w(final String TAG, final String message) {
        HiLogLabel hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x00201, TAG);
        return HiLog.warn(hiLogLabel, TAG, (message));
    }

    public static int inf(final String TAG, final String message) {
        HiLogLabel hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x00201, TAG);
        return HiLog.warn(hiLogLabel, TAG, (message));
    }

    public static int d(final String TAG, final String message) {
        HiLogLabel hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x00201, TAG);
        return HiLog.debug(hiLogLabel, TAG, (message));
    }

    public static int v(final String TAG, final String message) {
        HiLogLabel hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x00201, TAG);
        return HiLog.info(hiLogLabel, message);
    }


}
