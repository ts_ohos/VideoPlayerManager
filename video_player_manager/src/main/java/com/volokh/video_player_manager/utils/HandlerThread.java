package com.volokh.video_player_manager.utils;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.os.ProcessManager;

public class HandlerThread extends Thread {
    int mPriority;
    int mTid = -1;
    EventRunner mLooper;
    private EventHandler mHandler;
    private final static String TAG = HandlerThread.class.getSimpleName();
    public static final int THREAD_PRIORITY_DEFAULT = 0;

    public HandlerThread(String name) {
        super(name);
        mPriority = THREAD_PRIORITY_DEFAULT;
    }

    /**
     * Constructs a HandlerThread.
     *
     * @param name
     * @param priority The priority to run the thread at. The value supplied must be from
     */
    public HandlerThread(String name, int priority) {
        super(name);
        EventRunner.create(false);
        mPriority = priority;
    }

    /**
     * Call back method that can be explicitly overridden if needed to execute some
     * setup before Looper loops.
     */
    protected void onLooperPrepared() {
    }

    @Override
    public void run() {
        mTid = ProcessManager.getTid();
        synchronized (this) {
            mLooper = EventRunner.current();
            Logger.v(TAG, "mLooper =" + mLooper);
            notifyAll();
        }
        onLooperPrepared();
        ProcessManager.setThreadPriority(mPriority);
        // mLooper.run();
        mTid = -1;
    }


    /**
     * This method returns the Looper associated with this thread. If this thread not been started
     * or for any reason isAlive() returns false, this method will return null. If this thread
     * has been started, this method will block until the looper has been initialized.
     *
     * @return The looper.
     */
    public EventRunner getLooper() {
        if (!isAlive()) {
            return null;
        }

        // If the thread has been started, wait until the looper has been created.
        synchronized (this) {
            while (isAlive() && mLooper == null) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
        }
        return mLooper;
    }

    /**
     * @return a shared {@link EventHandler} associated with this thread
     * @hide
     */
    public EventHandler getThreadHandler() {
        if (mHandler == null) {
            mHandler = new EventHandler(getLooper());
        }
        return mHandler;
    }

    /**
     * Quits the handler thread's looper.
     * <p>
     * Causes the handler thread's looper to terminate without processing any
     * more messages in the message queue.
     * </p><p>
     * Any attempt to post messages to the queue after the looper is asked to quit will fail.
     * For example, the {@link EventHandler#sendEvent(int)} (Message)} method will return false.
     * </p><p class="note">
     * Using this method may be unsafe because some messages may not be delivered
     * before the looper terminates.  Consider using {@link #quitSafely} instead to ensure
     * that all pending work is completed in an orderly manner.
     * </p>
     *
     * @return True if the looper looper has been asked to quit or false if the
     * thread had not yet started running.
     * @see #quitSafely
     */
    public boolean quit() {
        EventRunner looper = getLooper();
        if (looper != null) {
            looper.stop();
            return true;
        }
        return false;
    }

    /**
     * Quits the handler thread's looper safely.
     * <p>
     * Causes the handler thread's looper to terminate as soon as all remaining messages
     * in the message queue that are already due to be delivered have been handled.
     * Pending delayed messages with due times in the future will not be delivered.
     * </p><p>
     * Any attempt to post messages to the queue after the looper is asked to quit will fail.
     * For example, the {@link EventHandler#sendEvent(InnerEvent)} (Message)} method will return false.
     * </p><p>
     * If the thread has not been started or has finished (that is if
     * {@link #getLooper} returns null), then false is returned.
     * Otherwise the looper is asked to quit and true is returned.
     * </p>
     *
     * @return True if the looper looper has been asked to quit or false if the
     * thread had not yet started running.
     */
    public boolean quitSafely() {
        EventRunner looper = getLooper();
        if (looper != null) {
            looper.stop();
            return true;
        }
        return false;
    }

    static final ThreadLocal<EventRunner> sThreadLocal = new ThreadLocal<>();

    private static void prepare(boolean quitAllowed) {
        if (sThreadLocal.get() != null) {
            throw new RuntimeException("Only one Looper may be created per thread");
        }
        //sThreadLocal.set();
    }

    /**
     * Returns the identifier of this thread. See Process.myTid().
     */
    public int getThreadId() {
        return mTid;
    }
}
