package com.volokh.video_player_manager.player_messages;



import com.volokh.video_player_manager.PlayerMessageState;
import com.volokh.video_player_manager.manager.VideoPlayerManagerCallback;
import com.volokh.video_player_manager.ui.VideoPlayerView;

/**
 * This PlayerMessage calls {@link ohos.media.player.Player#stop()} on the instance that is used inside {@link VideoPlayerView}
 */
public class Stop extends PlayerMessage {
    public Stop(VideoPlayerView videoView, VideoPlayerManagerCallback callback) {
        super(videoView, callback);
    }

    @Override
    protected void performAction(VideoPlayerView currentPlayer) {
        currentPlayer.stop();
    }

    @Override
    protected PlayerMessageState stateBefore() {
        return PlayerMessageState.STOPPING;
    }

    @Override
    protected PlayerMessageState stateAfter() {
        return PlayerMessageState.STOPPED;
    }
}
