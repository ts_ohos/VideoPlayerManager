package com.volokh.video_player_manager.player_messages;



import com.volokh.video_player_manager.manager.VideoPlayerManagerCallback;
import com.volokh.video_player_manager.ui.VideoPlayerView;
import ohos.global.resource.BaseFileDescriptor;
import ohos.global.resource.RawFileDescriptor;

/**
 * This PlayerMessage calls {@link ohos.media.player.Player#setSource(BaseFileDescriptor)} (FileDescriptor)} on the instance that is used inside {@link VideoPlayerView}
 */
public class SetAssetsDataSourceMessage extends SetDataSourceMessage{

    private final RawFileDescriptor mRawFileDescriptor;

    public SetAssetsDataSourceMessage(VideoPlayerView videoPlayerView, RawFileDescriptor rawFileDescriptor, VideoPlayerManagerCallback callback) {
        super(videoPlayerView, callback);
        mRawFileDescriptor = rawFileDescriptor;
    }

    @Override
    protected void performAction(VideoPlayerView currentPlayer) {
        currentPlayer.setDataSource(mRawFileDescriptor);
    }
}
