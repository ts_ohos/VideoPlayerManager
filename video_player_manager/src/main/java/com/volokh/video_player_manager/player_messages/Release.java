package com.volokh.video_player_manager.player_messages;



import com.volokh.video_player_manager.PlayerMessageState;
import com.volokh.video_player_manager.manager.VideoPlayerManagerCallback;
import com.volokh.video_player_manager.ui.VideoPlayerView;

/**
 * This PlayerMessage calls {@link ohos.media.player.Player#release()} on the instance that is used inside {@link VideoPlayerView}
 */
public class Release extends PlayerMessage {

    public Release(VideoPlayerView videoPlayerView, VideoPlayerManagerCallback callback) {
        super(videoPlayerView, callback);
    }

    @Override
    protected void performAction(VideoPlayerView currentPlayer) {
        currentPlayer.release();
    }

    @Override
    protected PlayerMessageState stateBefore() {
        return PlayerMessageState.RELEASING;
    }

    @Override
    protected PlayerMessageState stateAfter() {
        return PlayerMessageState.RELEASED;
    }
}
